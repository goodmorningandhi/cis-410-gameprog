# CIS410 - gp: Assignment 1 #

## Part 1: Roll A Ball

- Tutorial website: [https://learn.unity.com/tutorial/setting-up-the-game?uv=2019.4&projectId=5f158f1bedbc2a0020e51f0d#5f0fb872edbc2a001f18d417](https://learn.unity.com/tutorial/setting-up-the-game?uv=2019.4&projectId=5f158f1bedbc2a0020e51f0d#5f0fb872edbc2a001f18d417)

- Implementations: 
    - Roll a Ball

## Part 2: Double Jump

- Implementations:
    - Jump action bind with SPACE key
    - Check on the ground or not
    - OnJump function for checking on the air and jump counter, set jump force
    - FixedUpdate function for checking velocity of the rigidbody: if negitive, jump second time.
- Action
  When space key pressed, the ball's rigidbody got a upward force for jump. When the first jump downward to the ground, fixedupdate function will jump second time.

- Bugs:
  Sometimes will jump higher then normal, if the user rapidly press the space key multiple times.

## Team members:

- Liwei Fan    Email: `liweif@uoregon.edu`

## Contributions:

- Liwei: Part1 & Part2
