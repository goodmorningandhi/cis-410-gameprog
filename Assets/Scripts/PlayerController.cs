using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float speed = 0;
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;
    private bool isGround = true;
    private int jumps = 0;
    private float jumpSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winTextObject.SetActive(false);
        rb.velocity = new Vector3(0.0f, 0.0f, 0.0f);
    }

    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();
        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    void OnJump(InputValue value)
    {
        if (isGround || jumps < 2)
        {
            jumps++;
            jumpSpeed = 30.0f;
            isGround = false;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 movement = new Vector3(movementX, jumpSpeed, movementY);

        rb.AddForce(movement * speed);
        float upv = rb.velocity.y;
        if (upv < 0.0f && !isGround && (jumps < 2))
        {
            print("Second jump");
            jumps++;
            jumpSpeed = 30.0f;
            Vector3 secondjump = new Vector3(0.0f, jumpSpeed, 0.0f);
            rb.AddForce(secondjump * speed);
        }
        jumpSpeed = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        print(other);
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        if (other.gameObject.CompareTag("Ground"))
        {
            isGround = true;
            jumps = 0; 
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if (count >= 3)
        {
            winTextObject.SetActive(true);
        }

    }
}
